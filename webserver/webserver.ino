/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

/* načtení knihoven */
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>
#include <Wire.h>
#include "LittleFS.h"
//#include <ESP8266mDNS.h>

/* přihlášení na WiFi načtené z hlavičkového souboru */
#include "prihlaseni_wifi.h"
const char *ssid = STASSID;
const char *password = STAPSK;

/* nastavení konstant periferií */
const int led_deska = LED_BUILTIN;

/* stav LED */
char* ledState;

/* vytvoření instance AsyncWebServer na portu 80 */
AsyncWebServer server(80);

/* ukázka get metody, vrací náhodné číslo <0;100> */
String getRandom() {
  int nahodne = random(100);
  Serial.println(nahodne);
  return String(nahodne);
}

/* nastavení String ke zpětnému načtení do frontend */
String processor(const String& var){
  Serial.println(var);
  if(var == "STAV"){
    if(digitalRead(led_deska)){
      ledState = "ON";
    }
    else{
      ledState = "OFF";
    }
    Serial.print(ledState);
    return ledState;
  }
  else if (var == "RANDOM"){
    return getRandom();
  }
  return "";
}
 
/* funkce přepne stav LED na desce a zavolá handleRoot() */
void turn_led_deska() {
  digitalWrite(led_deska, !digitalRead(led_deska));  
}

void setup(){
  /* debug */
  Serial.begin(115200);

  /* nastavení LED_BUILTIN */
  pinMode(led_deska, OUTPUT);
  digitalWrite(led_deska, LOW);

  /* připojení LittleFS */
  if(!LittleFS.begin()){
    Serial.println("Při připojování LittleFS vznikla chyba");
    return;
  }

  /* připojení WiFi v modu STA*/
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  /* proces připojování na WiFi s finálním výpisem do konzole */
  Serial.println("");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("\nPřipojeno na: ");
  Serial.println(ssid);
  Serial.print("IP adresa: ");
  Serial.println(WiFi.localIP());
  /* spuštění Multicast DNS - přiděluji adresy ke jménům v malé síti */
  // if (MDNS.begin("esp8266")) {Serial.println("MDNS responder started");}

  /* ======== IMPORTANT ======== */
  /* nastavení přístupových bodů a s nimi svázaných funkcí */
  /* root / */
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/index.html", String(), false, processor);
  });
  /* /style.css */
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/style.css", "text/css");
  });
  /* /prepni - změna stavu LED */
  server.on("/prepni", HTTP_GET, [](AsyncWebServerRequest *request){
    turn_led_deska();
    request->send(LittleFS, "/index.html", String(), false, processor);
  });
  /* /random - načtení náhodné hodnoty */
  server.on("/random", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getRandom().c_str());
  });
  /* ======== IMPORTANT ======== */

  /* po nastavení můžeme spustit server a vypsat hlášku do konzole */
  server.begin();
}
 
void loop(){
}
