# ESP_fs_webserver

ukázkový kód k demonstraci použití Filesystem k uložení souborů pro webserver. MCU Wemos D1R1 (ESP 8266).

## Instalace

před samotným nahráváním kódu je potřeba [upravit funkčnost prostředí Arduino IDE](install/install.md).

## Nastavení velikosti FS

![nastavení velikosti FS](screenshots/arduinoIDE-velikostFS.png)
* v Arduino IDE pro nahrávání souborů na FS.
*FS ~ 3MB*

## Ukázkový kód

### Webserver FS - uložení souborů pro web frontend do externí složky
ukázka webserveru s FS se nachází v [webserver](webserver). Do podsložky `data` se umísťují soubory k nahrání na MCU. 
### FS timestamp
soubor pro [test ukládání na FS MCU](fs_timestamp) - do souboru ukládá `timestamp` a pak jej pro kontrolu načítá do konzole.

## FS - doplňující info
[ESP8266 FileSystem](https://arduino-esp8266.readthedocs.io/en/latest/filesystem.html)

## původní zdrojový kód
[ESP8266 webserver async](https://randomnerdtutorials.com/esp8266-web-server-spiffs-nodemcu/)

! jako filesystem použijte `LittleFS` místo `SPIFFS`, ten je **deprecated** 

---
* [Markdown cheatsheet - local](markdown-cheat-sheet.md)
* [Markdown cheatsheet - remote](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
